#DRAW_SIMPLE
###Very easy, simple software for creating drawings, generated in HTML Canvas.

#Instructions
##Shortcuts
###ESC
* breaks current drawing (before putting it onto canvas)
###SHIFT
* when held, it inverts colors
###CTRL
* (shapes, line) when held, it blocks shape drawing
* (draw) when held, it allows you to make curved line
###MOUSEWHEEL
* increases/decreases drawing width (except while holding ctrl - to allow website size change)
###RMB
* (shapes) draws with secondary color background
* (draw, line) inverts color
###KEYS
####1-7 
* changes drawing mode (respectively to its interface position)
####S
* saves picture (floppy icon does the same)
####Q,W,E
* opens color inputs (respectively: first color (left-top), second color (right-top), background color (bottom))
####R
* switches first and second color pernamently
####Z / Y
* undo / redo

##Color picker
* LMB - changes first color, RMB - second color; background is ignored (it should take black (#000000) color)
##Downloading image
* To save it click 's' or floppy disk icon. 
* Background is ignored, as preffered extension is PNG so it should be transparent.


#VERSION
Latest stable version: v2.0 (24-02-2019)

#CHANGE LOGS
v2.0 is first public version of the app.

#FUTURE
I think the project can be extended in the future as i have ideas for new features.
The target is to create software with many features, yet very easy to use because of keyboard binds and clear, basic interface

I'm glad to discuss every problems with app;
I'm also open for every suggestion. 

#CREDITS
##Author: Tuvrai
####And... big thanks for 'passive' help from this sources:
* http://kursjs.pl/kurs/canvas/canvas-paint.php - Marcin Domański's tutorial
* http://codetheory.in/creating-a-paint-application-with-html5-canvas/ - Rishabh's decent articles series

* Icons of interface features via: https://material.io/tools

#LICENSE
MIT